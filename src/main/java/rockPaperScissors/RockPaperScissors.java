package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.printf("Let's play round %d\n", roundCounter);
            String humanMove= readInput("Your choice (Rock/Paper/Scissors)?");
            String computerMove = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
            if (!rpsChoices.contains(humanMove)) {
                System.out.printf("I do not understand %s. Could you try again?\n",humanMove);
                continue;
            }
            else if(humanMove.equals(computerMove)) {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n",humanMove, computerMove);
            }
            else if(humanMove.equals("scissors") && computerMove.equals("paper") || humanMove.equals("paper") && computerMove.equals("rock") || humanMove.equals("rock") && computerMove.equals("scissors") ) {
                humanScore++;
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", humanMove, computerMove);
            }
            else {
                computerScore++;
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", humanMove, computerMove);
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            String continue_= readInput("Do you wish to continue playing? (y/n)?");
            while (true){

                if (!continue_.equals("n") && !continue_.equals("y")){
                    System.out.printf("I do not understand %s. Could you try again?",humanMove);
                    continue;
                }
                else if (continue_.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                }
                else{
                    break;
                }
            }
            if (continue_.equals("n")) {
                break;
            }
            roundCounter++;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
